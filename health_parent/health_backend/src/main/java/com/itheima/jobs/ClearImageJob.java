package com.itheima.jobs;

import com.itheima.controller.RedisConstant;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

public class ClearImageJob {

    @Autowired
    private JedisPool jedisPool;

    public void delete(){
        //清理图片
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println("delete----"+sdf.format(new Date()));
        //图片存储在两个集合当中
        // 1. 所有的上传图片 2. 套餐添加成功后的图片集合
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Set<String> sdiff = jedis.sdiff(RedisConstant.ALL_PIC, RedisConstant.DB_PIC);
            for (String fileName : sdiff) {
                //先从七牛云中删除
                QiniuUtils.delete(fileName);
                //从redis 所有的上传图片集合中删除
                jedis.srem(RedisConstant.ALL_PIC,fileName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (jedis != null){
                jedis.close();
            }
        }

    }

//    public void delete1(){
//        //清理图片
//        //清理图片
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println("delete1111111----"+sdf.format(new Date()));
//    }
}
