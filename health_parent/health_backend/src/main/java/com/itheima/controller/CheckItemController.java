package com.itheima.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.model.MessageConstant;
import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 体检检查项管理
 */
@RestController
@RequestMapping("/checkItem")
public class CheckItemController {
    //   axios.post("/checkItem/add.do",this.formData).then((res)=>{
    @Reference
    private CheckItemService checkItemService;
    @RequestMapping("add")
    public Result add(@RequestBody CheckItem checkItem){
        //交给service去处理业务逻辑
        try {
            return this.checkItemService.add(checkItem);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.ADD_CHECKITEM_FAIL);
    }
    // axios.post("/checkItem/findPage.do",this.pagination).then((res)=>{
    @RequestMapping("findPage")
    public Result findPage(@RequestBody QueryParams queryParams){
        //交给service去处理业务逻辑
        try {
            return this.checkItemService.findPage(queryParams);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_CHECKITEM_FAIL);
    }
    //  axios.get("/checkItem/findCheckItemById?id="+row.id).then((res)=>{
    @RequestMapping("findCheckItemById")
    public Result findCheckItemById(Integer id){
        //交给service去处理业务逻辑
        try {
            return this.checkItemService.findCheckItemById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_CHECKITEM_FAIL);
    }
    // axios.post("/checkItem/update.do",this.formData).then((res)=>{
    @RequestMapping("update")
    public Result update(@RequestBody CheckItem checkItem){
        //交给service去处理业务逻辑
        try {
            return this.checkItemService.update(checkItem);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.EDIT_CHECKITEM_FAIL);
    }
    // axios.get("/checkItem/delete.do?id="+row.id).then((res)=>{
    @RequestMapping("delete")
    public Result delete(@RequestParam("id") Integer checkItemId){
        //交给service去处理业务逻辑
        try {
            return this.checkItemService.delete(checkItemId);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.DELETE_CHECKITEM_FAIL);
    }


    @RequestMapping("findAll")
    public Result findAll(){
        //交给service去处理业务逻辑
        try {
            return this.checkItemService.findAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_CHECKITEM_FAIL);
    }

}
