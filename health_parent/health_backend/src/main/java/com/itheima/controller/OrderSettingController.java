package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.model.MessageConstant;
import com.itheima.model.Result;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateUtils;
import com.itheima.utils.POIUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("orderSetting")
public class OrderSettingController {

    @Reference
    private OrderSettingService orderSettingService;

    @RequestMapping("upload")
    public Result upload(@RequestParam("excelFile")MultipartFile file){
        try {
            List<String[]> list = POIUtils.readExcel(file);

            List<OrderSetting> orderSettingList = new ArrayList<OrderSetting>();
            //list[0] =orderDate  1=number
            if (list!=null){
                for (String[] strings : list) {
                    Date orderDate= null;
                    try {
                        orderDate = DateUtils.parseString2Date(strings[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (orderDate==null){
                        continue;
                    }
                    Integer number = Integer.parseInt(strings[1]);
                    OrderSetting orderSetting = new OrderSetting();
                    orderSetting.setOrderDate(orderDate);
                    orderSetting.setNumber(number);
                    orderSetting.setReservations(0);  //初始化
                    orderSettingList.add(orderSetting);
                }
               return this.orderSettingService.add(orderSettingList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.IMPORT_ORDERSETTING_FAIL);
    }

    //findOrderSettingByMonth
    @RequestMapping("findOrderSettingByMonth")
    public Result findOrderSettingByMonth(String month){
        try {
            return this.orderSettingService.findOrderSettingByMonth(month);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.GET_ORDERSETTING_FAIL);
    }

    @RequestMapping("updateNumber")
    public Result updateNumber(String date,Integer number){
        try {
            this.orderSettingService.updateNumber(date,number);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.GET_ORDERSETTING_FAIL);
    }
}
