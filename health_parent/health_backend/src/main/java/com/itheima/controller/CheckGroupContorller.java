package com.itheima.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.model.MessageConstant;
import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 体检检查项管理
 */
@RestController
@RequestMapping("/checkGroup")
public class CheckGroupContorller {
    //   axios.post("/checkItem/add.do",this.formData).then((res)=>{
    @Reference
    private CheckGroupService checkGroupService;

    @RequestMapping("add")
    public Result add(@RequestBody CheckGroup checkGroup){
        //交给service去处理业务逻辑
        try {
            return this.checkGroupService.add(checkGroup);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.ADD_CHECKGROUP_FAIL);

    }

    @RequestMapping("findPage")
    public Result findPage(@RequestBody QueryParams queryParams){
        //交给service去处理业务逻辑
        try {
            return this.checkGroupService.findPage(queryParams);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.ADD_CHECKGROUP_FAIL);


    }

    @RequestMapping("findById")
    public Result findById(Integer id){
        try {
            return this.checkGroupService.findById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_CHECKGROUP_FAIL);
    }

    //axios.post("/checkGroup/update.do",this.formData).then((res)=>{
    @RequestMapping("update")
    public Result update(@RequestBody CheckGroup checkGroup){
        try {
            return this.checkGroupService.update(checkGroup);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.EDIT_CHECKGROUP_FAIL);
    }

    //axios.get("/checkGroup/findAll.do").then((res)=>{
    @RequestMapping("findAll")
    public Result findAll(){
        try {
            return this.checkGroupService.findAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_CHECKGROUP_FAIL);
    }

    //delete
//    @RequestMapping("delete")
//    public Result delete(@RequestBody CheckGroup checkGroup){
//        try {
//            return this.checkGroupService.delete(checkGroup);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return Result.fail(MessageConstant.EDIT_CHECKGROUP_FAIL);
//    }
}