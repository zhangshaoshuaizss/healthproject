package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.model.MessageConstant;
import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.SetMeal;
import com.itheima.service.SetMealService;
import com.itheima.utils.QiniuUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("setMeal")
public class SetMealController {


    @Reference
    private SetMealService setMealService;

    //setmeal/upload.do"
    @RequestMapping("upload")
    public Result upload(@RequestParam("imgFile")  MultipartFile file){
        //1.jpg
        String originalFilename = file.getOriginalFilename();
        int lastIndexOf = originalFilename.lastIndexOf(".");
        System.out.println("lastIndexOf:"+lastIndexOf);
        String suffix = originalFilename.substring(lastIndexOf);
        System.out.println(suffix);
        String fileName = UUID.randomUUID().toString()+suffix;

        try {
            QiniuUtils.upload(file.getBytes(),fileName);
            String url = "http://qjifo4gqy.hb-bkt.clouddn.com/"+fileName;
            Map<String,String> map = new HashMap<String,String>();
            map.put("imageUrl",url);
            map.put("fileName",fileName);
            //图片上传成功，存储到redis中
//            Jedis jedis = null;
//            try {
//                jedis = jedisPool.getResource();
//                //set 集合
//                jedis.sadd(RedisConstant.ALL_PIC,fileName);
//            }catch (Exception e){
//                e.printStackTrace();
//            }finally {
//                if (jedis != null){
//                    jedis.close();
//                }
//            }


            return Result.success(MessageConstant.PIC_UPLOAD_SUCCESS,map);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.PIC_UPLOAD_FAIL);
    }

    @RequestMapping("add")
    public Result add(@RequestBody SetMeal setMeal) {
        try {
            return this.setMealService.add(setMeal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_SETMEAL_FAIL);


//        try {
//            Result result = this.setMealService.add(setMeal);
//            //套餐添加成功，将图片名称存入redis set集合中 代表已经存在数据库中的图片
//            if (result.isFlag()){
//                String fileName = setMeal.getImg();
//                Jedis jedis = null;
//                try {
//                    jedis = jedisPool.getResource();
//                    //set 集合
//                    jedis.sadd(RedisConstant.DB_PIC,fileName);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }finally {
//                    if (jedis != null){
//                        jedis.close();
//                    }
//                }
//            }
//            return result;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return Result.fail(MessageConstant.ADD_SETMEAL_FAIL);
    }

    @RequestMapping("findPage")
    public Result findPage(@RequestBody QueryParams queryParams) {
        try {
            return this.setMealService.findPage(queryParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_SETMEAL_FAIL);
    }


}
