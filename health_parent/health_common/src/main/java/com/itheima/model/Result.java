package com.itheima.model;

import java.io.Serializable;

public class Result implements Serializable {

    private boolean flag;

    private String message;

    private Object data;

    private int code;

    public Result(){

    }
    public Result(boolean flag,String message,Object data){
        this.flag = flag;
        this.message = message;
        this.data = data;
    }
    public static Result fail(String message) {
        return new Result(false,message,null);
    }

    public static Result success(String message,Object data) {
        return new Result(true,message,data);
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
