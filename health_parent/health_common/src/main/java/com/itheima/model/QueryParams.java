package com.itheima.model;

import java.io.Serializable;

public class QueryParams implements Serializable {
    //currentPage: 1,//当前页码
    //					  pageSize:10,//每页显示的记录数
    //					  total:0,//总记录数
    //					  queryString:null//查询条件

    private Integer currentPage;
    private Integer pageSize;
    private String queryString;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
}
