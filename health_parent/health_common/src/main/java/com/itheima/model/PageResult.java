package com.itheima.model;

import java.io.Serializable;
import java.util.List;

public class PageResult implements Serializable {

    private Long total;

    private List dataList;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List getDataList() {
        return dataList;
    }

    public void setDataList(List dataList) {
        this.dataList = dataList;
    }
}
