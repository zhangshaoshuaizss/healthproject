package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckItemDao;
import com.itheima.model.MessageConstant;
import com.itheima.model.PageResult;
import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 检查项服务
 */
@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;

    public Result add(CheckItem checkItem) {
        checkItemDao.save(checkItem);
        System.out.println("id:"+checkItem.getId());
        return Result.success(MessageConstant.ADD_CHECKITEM_SUCCESS,null);
    }


    public Result findPage(QueryParams queryParams) {
        //使用mybatis的分页插件 来实现分页
        PageHelper.startPage(queryParams.getCurrentPage(),queryParams.getPageSize());
        Page<CheckItem> checkItemPage = checkItemDao.selectByCondition(queryParams.getQueryString());
        long total = checkItemPage.getTotal();
        List<CheckItem> checkItemList = checkItemPage.getResult();
        //封装分页对象
        ////返回值: res.data=Result -> data (total,dataList)
        PageResult pageResult = new PageResult();
        pageResult.setTotal(total);
        pageResult.setDataList(checkItemList);

        return Result.success(MessageConstant.QUERY_CHECKITEM_SUCCESS,pageResult);
    }


    public Result findCheckItemById(Integer checkItemId) {
        CheckItem checkItem = this.checkItemDao.findCheckItemById(checkItemId);
        return Result.success(MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItem);
    }


    public Result update(CheckItem checkItem) {
        this.checkItemDao.update(checkItem);
        return Result.success(MessageConstant.EDIT_CHECKITEM_SUCCESS,null);
    }


    public Result delete(Integer checkItemId) {
        //1. 判断 如果检查项被使用，那么应该提示用户，此检查项不能被删除
        //select count(0) from t_checkgroup_checkitem where checkitem_id=#{checkItemId} > 0 不能被删除
        int count = this.checkItemDao.countCheckGroupAndItemByItemId(checkItemId);
        if (count > 0){
            //业务逻辑错误
            return Result.fail("此检查项已经被使用，不能被删除");
        }
        //2. 如果没有被使用，可以删除
        this.checkItemDao.delete(checkItemId);
        return Result.success(MessageConstant.DELETE_CHECKITEM_SUCCESS,null);
    }

    public Result findAll() {
        List<CheckItem> checkItemList=this.checkItemDao.findAll();
        return Result.success(MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkItemList);
    }
}
