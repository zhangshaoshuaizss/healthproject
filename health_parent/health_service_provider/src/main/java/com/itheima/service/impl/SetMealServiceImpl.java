package com.itheima.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.SetMealDao;
import com.itheima.model.MessageConstant;
import com.itheima.model.PageResult;
import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.SetMeal;
import com.itheima.service.SetMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service(interfaceClass = SetMealService.class)
@Transactional
public class SetMealServiceImpl implements SetMealService {

    public static final String imageUrl = "http://qjifo4gqy.hb-bkt.clouddn.com/";
    @Autowired
    private SetMealDao setMealDao;
    @Autowired
    private CheckGroupDao checkGroupDao;

    public Result add(SetMeal setMeal) {
//        //1. add t_setmeal  save方法应该将id赋值
        this.setMealDao.save(setMeal);
//        //2. add t_setmeal_checkgroup
        Integer setMealId = setMeal.getId();
        List<Integer> checkGroupIds = setMeal.getCheckGroupIds();
        for (Integer checkGroupId : checkGroupIds) {
            this.setMealDao.saveSetMealAndGroup(setMealId,checkGroupId);
        }
        return Result.success(MessageConstant.ADD_SETMEAL_SUCCESS,null);
    }

    @Override
    public Result findPage(QueryParams queryParams) {
        //mybatis的分页
        PageHelper.startPage(queryParams.getCurrentPage(),queryParams.getPageSize());
        //select * from t_setmeal 查询条件
        Page<SetMeal> setMealPage = this.setMealDao.selectByCondition(queryParams.getQueryString());
        PageResult pageResult = new PageResult();
        pageResult.setDataList(setMealPage.getResult());
        pageResult.setTotal(setMealPage.getTotal());
        return Result.success(MessageConstant.QUERY_SETMEAL_SUCCESS,pageResult);
    }

    @Override
    public Result findAll() {
        List<SetMeal> setMealList = this.setMealDao.findAll();
        for (SetMeal setMeal : setMealList) {
            //给图片 加上前缀 便于访问
            setMeal.fillImg(imageUrl);
        }
        return Result.success(MessageConstant.GET_SETMEAL_LIST_SUCCESS,setMealList);
    }


//    @Autowired
//    private JedisPool jedisPool;
//    @Override
//    public Result findAll() {
//        Jedis jedis=null;
//        List<SetMeal> setMealList=new ArrayList<SetMeal>();
//        try{
//            jedis=jedisPool.getResource();
//
//            String setmealall = jedis.set("SET_MEAL_ALL",null);
//            if (!StringUtil.isEmpty(setmealall)){
//                List<SetMeal> setMeals = JSON.parseArray(setmealall, SetMeal.class);
//                return Result.success(MessageConstant.GET_SETMEAL_LIST_SUCCESS,setMeals);
//            }else {
//             setMealList = this.setMealDao.findAll();
//            for (SetMeal setMeal : setMealList) {
//                //给图片 加上前缀 便于访问
//                setMeal.fillImg(imageUrl);
//                jedis.setex("SET_MEAL_ALL",5*60,JSON.toJSONString(setMeal));
//            }}
//        }catch (Exception e){
//            e.printStackTrace();
//
//        }finally {
//            if (jedis!=null){
//                jedis.close();
//            }
//        }
//        return Result.success(MessageConstant.GET_SETMEAL_LIST_SUCCESS,setMealList);
//
//    }

    @Override
    public Result findById(Integer setMealId){
        //1. 查询套餐表中数据
        SetMeal setMeal = this.setMealDao.findById(setMealId);
        if (setMeal == null){
            return Result.fail("套餐id不存在");
        }
        setMeal.fillImg(imageUrl);
        //2. 根据套餐id和检查组的关联关系，查询对应的检查组列表
//        List<CheckGroup> checkGroupList = this.setMealDao.findCheckGroupListByMealId(setMealId);
////        //3. 根据检查组和检查项的关联关系，查询对应的检查项列表
////        for (CheckGroup checkGroup : checkGroupList) {
////            List<CheckItem> checkItemList = this.checkGroupDao.findCheckItemListByGroupId(checkGroup.getId());
////            checkGroup.setCheckItems(checkItemList);
////        }
////        setMeal.setCheckGroups(checkGroupList);
        return Result.success(MessageConstant.QUERY_SETMEAL_SUCCESS,setMeal);
    }

    @Override
    public List<Map<String, Object>> findSetmealCount() {
        return setMealDao.findSetmealCount();
    }
}
