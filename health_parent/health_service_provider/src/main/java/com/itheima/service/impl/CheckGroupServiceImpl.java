package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.model.MessageConstant;
import com.itheima.model.PageResult;
import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {
    @Autowired
    private CheckGroupDao checkGroupDao;
    @Autowired
    private CheckItemDao checkItemDao;
    @Override
    public Result add(CheckGroup checkGroup) {
        //1. 添加t_checkgroup  save方法 checkGroup的id 需要赋值
        this.checkGroupDao.save(checkGroup);
        //2. 添加 t_checkgroup_checkitem
        Integer checkGroupId = checkGroup.getId();
        saveCheckGroupAndCheckItem(checkGroup, checkGroupId);
        return Result.success(MessageConstant.ADD_CHECKGROUP_SUCCESS,null);
    }

    @Override
    public Result findPage(QueryParams queryParams) {
        //mybatis的分页
        PageHelper.startPage(queryParams.getCurrentPage(),queryParams.getPageSize());
        Page<CheckGroup> checkGroupPage = this.checkGroupDao.selectByCondition(queryParams.getQueryString());
        //Result -> data ->PageResult(total,dataList)
        PageResult pageResult = new PageResult();
        pageResult.setTotal(checkGroupPage.getTotal());
        pageResult.setDataList(checkGroupPage.getResult());
        return Result.success(MessageConstant.QUERY_CHECKGROUP_SUCCESS,pageResult);
    }

    @Override
    public Result findById(Integer checkGroupId) {
        //回显检查项列表
        List<CheckItem> checkItemList = this.checkItemDao.findAll();
        //回显选中的状态
        //select checkitem_id from t_checkgroup_checkitem where checkgroup_id=#{checkGroupId}
        List<Integer> checkItemIdList = this.checkGroupDao.findItemIdsByGroupId(checkGroupId);
        //回显基本信息
        CheckGroup checkGroup = this.checkGroupDao.findById(checkGroupId);
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("checkItemList",checkItemList);
        map.put("checkItemIdList",checkItemIdList);
        map.put("checkGroup",checkGroup);
        return Result.success(MessageConstant.QUERY_CHECKGROUP_SUCCESS,map);
    }

    @Override
    public Result update(CheckGroup checkGroup) {
        //1. 基本信息 更新 t_checkgroup
        this.checkGroupDao.update(checkGroup);
        //2. 选中的检查项，t_checkgroup_checkitem
        //3. 删除原有的 检查组和检查项的关联关系，重新建立
        Integer checkGroupId = checkGroup.getId();
        this.checkGroupDao.deleteCheckGroupAndItem(checkGroupId);
        saveCheckGroupAndCheckItem(checkGroup, checkGroupId);
        return Result.success(MessageConstant.EDIT_CHECKGROUP_SUCCESS,null);
    }

    public Result findAll() {
        List<CheckGroup> checkGroups=this.checkGroupDao.findAll();
        return Result.success(MessageConstant.EDIT_CHECKGROUP_SUCCESS,checkGroups);
    }

    //检查组和检查项的关联关系的建立，抽取方法
    private void saveCheckGroupAndCheckItem(CheckGroup checkGroup, Integer checkGroupId) {
        List<Integer> checkItemIds = checkGroup.getCheckitemIds();
        for (Integer checkItemId : checkItemIds) {
            this.checkGroupDao.saveCheckGroupAndItem(checkGroupId, checkItemId);
        }
    }
}
