package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.OrderSettingDao;
import com.itheima.model.MessageConstant;
import com.itheima.model.Result;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {
    @Autowired
    private OrderSettingDao orderSettingDao;

    @Override
    public Result add(List<OrderSetting> orderSettingList) {
        for (OrderSetting orderSetting : orderSettingList) {
            //数据库保存 orderSetting
            // 万一之前已经导入过 怎么办？所在日期的预约数量能不能重复导入
            // 要保证可以进行重复的导入操作
            //如果 日期已经有数据了，那么做update操作
            OrderSetting dbOrderSetting = this.orderSettingDao.findOrderSettingByDate(orderSetting.getOrderDate());
            if (dbOrderSetting == null){
                //save
                this.orderSettingDao.save(orderSetting);
            }else{
                dbOrderSetting.setNumber(orderSetting.getNumber());
                this.orderSettingDao.updateNumber(dbOrderSetting);
            }
        }
        return Result.success(MessageConstant.IMPORT_ORDERSETTING_SUCCESS,null);
    }

    @Override
    public Result findOrderSettingByMonth(String month) {
        String start = month+"-01";
        String end = month+"-31";
        List<OrderSetting> orderSettingList = orderSettingDao.findOrderSettingBetween(start,end);
        System.out.println(orderSettingList);
        List<Map<String,Integer>> list = new ArrayList<Map<String,Integer>>();
        for (OrderSetting orderSetting : orderSettingList) {
            Map<String,Integer> map = new HashMap<String,Integer>();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(orderSetting.getOrderDate());
            map.put("date",calendar.get(Calendar.DAY_OF_MONTH));
            map.put("number",orderSetting.getNumber());
            map.put("reservations",orderSetting.getReservations());
            list.add(map);
        }
        return Result.success(MessageConstant.GET_ORDERSETTING_SUCCESS,list);
    }

    @Override
    public Result updateNumber(String date, Integer number) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date parse = sdf.parse(date);
        OrderSetting orderSetting=new OrderSetting(parse,number);
        OrderSetting dbOrderSetting = this.orderSettingDao.findOrderSettingByDate(orderSetting.getOrderDate());
        if (dbOrderSetting==null){
            this.orderSettingDao.save(orderSetting);
        }else {
            this.orderSettingDao.updateNumberByDate(number,date);
        }
        return Result.success(MessageConstant.GET_ORDERSETTING_SUCCESS,null);
    }
}
