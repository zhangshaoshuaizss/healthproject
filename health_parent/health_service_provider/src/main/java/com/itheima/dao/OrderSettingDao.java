package com.itheima.dao;

import com.itheima.pojo.OrderSetting;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderSettingDao {

    OrderSetting findOrderSettingByDate(Date orderDate);

    void save(OrderSetting dbOrderSetting);

    void updateNumber(OrderSetting dbOrderSetting);

    List<OrderSetting> findOrderSettingBetween(@Param("start") String start,
                                               @Param("end") String end);

    void updateNumberByDate(@Param("number")Integer number,
                            @Param("date") String date);

    OrderSetting findByOrderDate(Date date);

    void editReservationsByOrderDate(OrderSetting orderSetting);
}
