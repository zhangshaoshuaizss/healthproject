package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CheckItemDao {

    void save(CheckItem checkItem);

    Page<CheckItem> selectByCondition(@Param("queryString") String queryString);

    CheckItem findCheckItemById(Integer checkItemId);

    void update(CheckItem checkItem);

    int countCheckGroupAndItemByItemId(Integer checkItemId);

    void delete(Integer checkItemId);

    List<CheckItem> findAll();

    List<CheckItem> findCheckItemListByGroupId(Integer checkGroupId);
}
