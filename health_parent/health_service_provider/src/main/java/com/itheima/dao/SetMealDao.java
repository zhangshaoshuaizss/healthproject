package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.SetMeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SetMealDao {
    void save(SetMeal setmeal);

    void saveSetMealAndGroup(@Param("setMealId") Integer setMealId,
                             @Param("checkGroupId") Integer checkGroupId);

    Page<SetMeal> selectByCondition(@Param("queryString") String queryString);

    List<SetMeal> findAll();

    SetMeal findById(Integer setMealId);

    List<Map<String, Object>> findSetmealCount();
}
