package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CheckGroupDao {
    void save(CheckGroup checkGroup);

    void saveCheckGroupAndItem(@Param("checkGroupId") Integer checkGroupId,
                               @Param("checkItemId") Integer checkItemId);

    Page<CheckGroup> selectByCondition(@Param("queryString") String queryString); //默认用 value是可以取值的

    List<Integer> findItemIdsByGroupId(Integer checkGroupId);

    CheckGroup findById(Integer checkGroupId);

    void update(CheckGroup checkGroup);

    void deleteCheckGroupAndItem(Integer checkGroupId);

    List<CheckGroup> findAll();

    List<CheckGroup> findCheckGroupListByMealId(Integer setMealId);
}

