package com.itheima.service;

import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.CheckGroup;

public interface CheckGroupService {
    Result add(CheckGroup checkGroup);

    Result findPage(QueryParams queryParams);

    Result findById(Integer checkGroupId);

    Result update(CheckGroup checkGroup);

    Result findAll();
}
