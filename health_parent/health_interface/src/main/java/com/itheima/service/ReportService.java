package com.itheima.service;

import java.util.Map;

/**
 * @author :郭俊伟E
 * @date :
 */
public interface ReportService {
    Map<String, Object> getBusinessReport() throws Exception;
}
