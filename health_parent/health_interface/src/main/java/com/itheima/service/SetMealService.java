package com.itheima.service;

import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.SetMeal;

import java.util.List;
import java.util.Map;

public interface SetMealService {
    Result add(SetMeal setmeal);

    Result findPage(QueryParams queryParams);

    Result findAll();

    Result findById(Integer id);

    List<Map<String, Object>> findSetmealCount();
}
