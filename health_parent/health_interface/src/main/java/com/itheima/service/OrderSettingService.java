package com.itheima.service;

import com.itheima.model.Result;
import com.itheima.pojo.OrderSetting;

import java.text.ParseException;
import java.util.List;

public interface OrderSettingService {
    Result add(List<OrderSetting> orderSettings);

    Result findOrderSettingByMonth(String month);

    Result updateNumber(String date, Integer number) throws ParseException;
}
