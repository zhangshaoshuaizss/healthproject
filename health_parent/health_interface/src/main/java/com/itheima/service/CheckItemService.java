package com.itheima.service;

import com.itheima.model.QueryParams;
import com.itheima.model.Result;
import com.itheima.pojo.CheckItem;

public interface CheckItemService {
    Result add(CheckItem checkItem);

    Result findPage(QueryParams queryParams);

    Result findCheckItemById(Integer checkItemId);

    Result update(CheckItem checkItem);

    Result delete(Integer checkItemId);

    Result findAll();
}
