package com.itheima.controller;

import com.itheima.model.Result;
import com.itheima.utils.SMSUtils;
import com.itheima.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@RestController
@RequestMapping("validateCode")
public class ValidateCodeController {

    @Autowired
    private JedisPool jedisPool;

    //axios.get("/validateCode/sendCode.do?telephone="+telephone).
    @RequestMapping("sendCode")
    public Result sendCode(String telephone){
        //验证码得需要生成
        Integer code = ValidateCodeUtils.generateValidateCode(4);
        //1. 拿到手机号，发送验证码 ，验证码得需要生成
        Jedis jedis = null;
        try {
            SMSUtils.sendSms(telephone,String.valueOf(code));
            //2. 发送成功之后，验证码存储到redis当中，redis需要设置过期时间
            jedis = jedisPool.getResource();
            jedis.setex("ORDER_SETTING_"+telephone,60*5,String.valueOf(code));
            return Result.success("发送验证码成功",null);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (jedis != null){
                jedis.close();
            }
        }
        return Result.fail("验证码发送失败");
    }
}
