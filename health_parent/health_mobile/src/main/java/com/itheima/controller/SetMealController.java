package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.model.MessageConstant;
import com.itheima.model.Result;
import com.itheima.service.SetMealService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("setMeal")
public class SetMealController {

    @Reference
    private SetMealService setMealService;

    // axios.post("/setMeal/getSetMeal.do").then((response)=>{
    @RequestMapping("getSetMeal")
    public Result getSetMeal(){
        try {
            return setMealService.findAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.GET_SETMEAL_LIST_FAIL);
    }

    //findById
    @RequestMapping("findById")
    public Result findById(Integer id){
        try {

            return this.setMealService.findById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.fail(MessageConstant.QUERY_SETMEAL_FAIL);
    }
}
